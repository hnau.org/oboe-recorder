package org.hnau.oboerecorder.domain.interactors.listen

import org.mockito.Mockito
import org.mockito.stubbing.OngoingStubbing


inline fun <reified T> mock(
    config: T.() -> Unit = {}
): T = Mockito
    .mock(T::class.java)
    .apply(config)

fun <T> whenever(methodCall: T): OngoingStubbing<T> =
    Mockito.`when`(methodCall)

fun <T> any(
    placeholder: T
): T {
    Mockito.any<T>()
    return placeholder
}