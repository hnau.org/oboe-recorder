package org.hnau.oboerecorder.utils

import io.reactivex.rxjava3.disposables.Disposable


inline fun <R> withDisposable(
    disposable: Disposable,
    action: () -> R
): R {
    val result = action()
    disposable.dispose()
    return result
}