package org.hnau.oboerecorder.domain.interactors

import io.reactivex.rxjava3.core.Observable
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.hnau.oboerecorder.domain.entity.record.AvailableRecordAction


fun AvailableRecordAction.executeRequired(
    requiredType: AvailableRecordAction.Type
) {
    MatcherAssert.assertThat(type, CoreMatchers.equalTo(requiredType))
    action?.invoke()
}

fun Observable<AvailableRecordAction>.executeRequired(
    requiredType: AvailableRecordAction.Type
) {
    var executedFirstAction = false
    subscribe { action ->
        if (executedFirstAction) {
            return@subscribe
        }
        action.executeRequired(requiredType)
        executedFirstAction = true
    }.dispose()
}