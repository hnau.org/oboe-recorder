package org.hnau.oboerecorder.domain.interactors.listen

import org.hnau.oboerecorder.domain.entity.listen.RecordedFileStatus
import org.hnau.oboerecorder.domain.repository.RecordFileRepository


fun TestRecordFileRepository(
    listenable: Boolean,
    onListened: () -> Unit = {}
) = mock<RecordFileRepository>() {
    whenever(status).thenReturn(
        if (listenable) {
            RecordedFileStatus.Exists(onListened)
        } else {
            RecordedFileStatus.NotExists
        }
    )
}