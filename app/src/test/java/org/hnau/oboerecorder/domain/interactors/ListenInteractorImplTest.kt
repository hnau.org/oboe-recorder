package org.hnau.oboerecorder.domain.interactors

import org.hnau.oboerecorder.domain.entity.listen.AvailableListenAction
import org.hnau.oboerecorder.domain.entity.listen.RecordedFileStatus
import org.hnau.oboerecorder.domain.entity.record.AvailableRecordAction
import org.hnau.oboerecorder.domain.interactors.listen.ListenInteractorImpl
import org.hnau.oboerecorder.domain.interactors.listen.TestRecordingStatusProvider
import org.hnau.oboerecorder.domain.interactors.listen.mock
import org.hnau.oboerecorder.domain.interactors.listen.whenever
import org.hnau.oboerecorder.domain.interactors.record.RecordInteractorImpl
import org.hnau.oboerecorder.domain.repository.RecordFileRepository
import org.junit.Test

class ListenInteractorImplTest {

    @Test
    fun testNewFileToListen() {

        val recordFileRepository = mock<RecordFileRepository>()

        whenever(recordFileRepository.status).thenReturn(RecordedFileStatus.NotExists)

        val recordingStatusProvider =
            TestRecordingStatusProvider(true)

        val recordInteractor = RecordInteractorImpl(
            recordingStatusProvider
        )

        val listenInteractor = ListenInteractorImpl(
            recordingStatusProvider,
            recordFileRepository
        )

        val availableActionObserver =
            listenInteractor.availableAction.map(AvailableListenAction::type).test()

        recordInteractor.availableAction.executeRequired(AvailableRecordAction.Type.start)

        whenever(recordFileRepository.status).thenReturn(RecordedFileStatus.Exists {})

        recordInteractor.availableAction.executeRequired(AvailableRecordAction.Type.stop)

        availableActionObserver.assertValues(
            AvailableListenAction.Type.none,
            AvailableListenAction.Type.listen
        )

    }

    @Test
    fun testUpdatedFileToListen() {

        val recordFileRepository = mock<RecordFileRepository>()

        whenever(recordFileRepository.status).thenReturn(RecordedFileStatus.Exists {})

        val recordingStatusProvider =
            TestRecordingStatusProvider(true)

        val recordInteractor = RecordInteractorImpl(
            recordingStatusProvider
        )

        val listenInteractor = ListenInteractorImpl(
            recordingStatusProvider,
            recordFileRepository
        )

        val availableActionObserver =
            listenInteractor.availableAction.map(AvailableListenAction::type).test()

        recordInteractor.availableAction.executeRequired(AvailableRecordAction.Type.start)

        recordInteractor.availableAction.executeRequired(AvailableRecordAction.Type.stop)

        availableActionObserver.assertValues(
            AvailableListenAction.Type.listen,
            AvailableListenAction.Type.none,
            AvailableListenAction.Type.listen
        )

    }

}