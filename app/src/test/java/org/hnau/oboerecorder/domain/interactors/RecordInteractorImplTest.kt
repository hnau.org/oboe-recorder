package org.hnau.oboerecorder.domain.interactors

import org.hnau.oboerecorder.domain.entity.record.AvailableRecordAction
import org.hnau.oboerecorder.domain.entity.record.RecordingStatusView
import org.hnau.oboerecorder.domain.interactors.listen.TestRecordingStatusProvider
import org.hnau.oboerecorder.domain.interactors.record.RecordInteractorImpl
import org.junit.Assert
import org.junit.Test

class RecordInteractorImplTest {

    @Test
    fun testSuccess() {

        val recordInteractor = RecordInteractorImpl(
            TestRecordingStatusProvider(true)
        )

        val isRecordingObserver =
            recordInteractor.isRecording.test()

        val statusViewObserver =
            recordInteractor.statusView.test()

        val availableActionObserver =
            recordInteractor.availableAction.map(AvailableRecordAction::type).test()

        recordInteractor.availableAction.executeRequired(AvailableRecordAction.Type.start)
        recordInteractor.availableAction.executeRequired(AvailableRecordAction.Type.stop)

        isRecordingObserver.assertValues(
            false,
            true,
            false
        )

        statusViewObserver.assertValues(
            RecordingStatusView.ready,
            RecordingStatusView.preparing,
            RecordingStatusView.recording,
            RecordingStatusView.ready
        )

        availableActionObserver.assertValues(
            AvailableRecordAction.Type.start,
            AvailableRecordAction.Type.none,
            AvailableRecordAction.Type.stop,
            AvailableRecordAction.Type.start
        )

    }

    @Test
    fun testRecordingError() {

        val recordInteractor = RecordInteractorImpl(
            TestRecordingStatusProvider(false)
        )

        val isRecordingObserver =
            recordInteractor.isRecording.test()

        val statusViewObserver =
            recordInteractor.statusView.test()

        val availableActionObserver =
            recordInteractor.availableAction.map(AvailableRecordAction::type).test()

        recordInteractor.availableAction.executeRequired(AvailableRecordAction.Type.start)

        isRecordingObserver.assertValues(
            false,
            true,
            false
        )

        statusViewObserver.assertValues(
            RecordingStatusView.ready,
            RecordingStatusView.preparing,
            RecordingStatusView.recording,
            RecordingStatusView.error
        )

        availableActionObserver.assertValues(
            AvailableRecordAction.Type.start,
            AvailableRecordAction.Type.none,
            AvailableRecordAction.Type.stop,
            AvailableRecordAction.Type.start
        )

    }

    @Test
    fun testPrepareMicError() {

        val error = Throwable()
        var catchedOnce = false

        val recordInteractor = RecordInteractorImpl(
            TestRecordingStatusProvider(
                successRecording = true,
                userMessagesErrorsReceiver = { receiverError ->
                    if (catchedOnce) {
                        Assert.fail("Already cached error")
                    }
                    Assert.assertEquals(error, receiverError)
                    catchedOnce = true
                },
                getPrepareMicThrowable = {
                    error
                }
            )
        )

        recordInteractor.availableAction.executeRequired(AvailableRecordAction.Type.start)

        Assert.assertTrue("Error was not catched", catchedOnce)
    }

}