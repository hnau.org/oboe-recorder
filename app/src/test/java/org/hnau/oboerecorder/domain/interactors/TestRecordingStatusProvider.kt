package org.hnau.oboerecorder.domain.interactors.listen

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import org.hnau.oboerecorder.domain.entity.record.RecordStatus
import org.hnau.oboerecorder.domain.interactors.RecordingStatusProvider
import org.hnau.oboerecorder.domain.repository.PrepareMicRepository
import org.hnau.oboerecorder.domain.repository.RecordRepository
import org.hnau.oboerecorder.domain.repository.UIScopeRepository
import org.hnau.oboerecorder.domain.repository.UserMessagesRepository


fun TestRecordingStatusProvider(
    successRecording: Boolean,
    userMessagesErrorsReceiver: (Throwable) -> Unit = {},
    getPrepareMicThrowable: () -> Throwable? = { null },
) = RecordingStatusProvider(
    prepareMicRepository = mock<PrepareMicRepository> {
        whenever(prepareMic()).thenReturn(getPrepareMicThrowable()?.let { Completable.error(it) }
            ?: Completable.complete())
    },
    recordRepository = mock<RecordRepository> {
        var updateStatus: ((RecordStatus) -> Unit)? = null
        var readyStatus: RecordStatus.Ready? = null
        val recordingStatus = RecordStatus.Recording(
            stop = {
                updateStatus!!(readyStatus!!)
            }
        )
        readyStatus = RecordStatus.Ready(
            previousRecordingIsSuccess = successRecording,
            start = {
                updateStatus!!(RecordStatus.Preparing)
                updateStatus!!(recordingStatus)
                if (!successRecording) {
                    updateStatus!!(readyStatus!!)
                }
            }
        )
        val statusMock = BehaviorSubject.createDefault<RecordStatus>(
            readyStatus.copy(previousRecordingIsSuccess = true)
        )
        updateStatus = statusMock::onNext
        whenever(status).thenReturn(statusMock)
    },
    userMessagesRepository = mock<UserMessagesRepository> {
        whenever(showError(any(Throwable())))
            .then {
                val error = it.getArgument<Throwable>(0)
                userMessagesErrorsReceiver(error)
            }
    },
    uiScopeRepository = mock<UIScopeRepository> {
        whenever(uiScopeDisposable).thenReturn(CompositeDisposable())
    }

)