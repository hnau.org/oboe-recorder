#ifndef OBOERECORDER_OBOERECORDER_H
#define OBOERECORDER_OBOERECORDER_H

#include <memory>

#include "MicListener.h"

namespace audio {

    class OboeRecorder {

    public:
        OboeRecorder(
                const char *filePath
        );

        bool recordSync();

        bool stop();

    private:
        const char *filePath_;
        MicListener micListener;

    };

}


#endif //OBOERECORDER_OBOERECORDER_H
