#ifndef OBOERECORDER_MICLISTENER_H
#define OBOERECORDER_MICLISTENER_H

#include <cstdio>
#include <functional>
#include <oboe/Oboe.h>

#include "Audio.h"

namespace audio {

    class MicListener {

    public:

        typedef std::function<void(Value, SamplesPerSecond)> OnNextValueCallback;
        typedef std::function<void()> OnFinishedCallback;

    public:

        //Read audio from microphone synchronisous
        bool readSync(
                const OnNextValueCallback &onNextValue, //Handle next value from mic
                const OnFinishedCallback &onStopped //Handle stream end
        );

        //Stop reading audio
        bool stop();

    private:
        bool isActive = true;
        std::mutex isActiveSyncMutex_;

    private:

        static bool prepare(
                std::shared_ptr<oboe::AudioStream> &stream
        );

        bool read(
                oboe::AudioStream *stream,
                const OnNextValueCallback &onNextValue,
                const OnFinishedCallback &onStopped
        );

        static bool readNext(
                oboe::AudioStream *stream,
                Value *buffer,
                size_t bufferSize,
                SamplesPerSecond samplesPerSecond,
                const OnNextValueCallback &onNextValue
        );

        static bool finish(
                oboe::AudioStream *stream
        );

    };

}


#endif //OBOERECORDER_MICLISTENER_H
