#ifndef OBOERECORDER_SOUNDEFFECT_H
#define OBOERECORDER_SOUNDEFFECT_H

#include "Audio.h"

namespace audio {

    class SoundEffect {

    public:

        virtual ~SoundEffect() {};

        //value is current audio value
        //returns value modified by effect
        virtual Value apply(Value value, SamplesPerSecond samplesPerSecond) = 0;

    };

}


#endif //OBOERECORDER_SOUNDEFFECT_H
