#ifndef OBOERECORDER_WAVWRITER_H
#define OBOERECORDER_WAVWRITER_H

#include <iostream>

#include "Audio.h"


namespace audio {

    class WavWriter {

    public:

        WavWriter(
                std::shared_ptr<std::ofstream> destination //WAV file path
        );

        //Write next value to WAV file
        void onNextValue(
                Value value,
                SamplesPerSecond samplesPerSecond
        );

        //Finish WAB file filling
        void onNoMoreValues();


    private:
        std::shared_ptr<std::ofstream> destination_;
        size_t dataChunkPos_ = 0;
        bool headerWasWritten_ = false;

    private:

        void writeHeader(size_t samplesPerSecond);

        void fillSizes();

    };

}


#endif //OBOERECORDER_WAVWRITER_H
