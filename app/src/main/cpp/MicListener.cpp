#include "MicListener.h"

#include <sstream>
#include "LoggingMacros.h"

using namespace audio;

namespace {

    constexpr DurationMS onePortionDurationMs = 1;

}

bool MicListener::readSync(
        const OnNextValueCallback &onNextValue,
        const MicListener::OnFinishedCallback &onStopped
) {

    std::shared_ptr<oboe::AudioStream> stream;

    if (!prepare(stream)) {
        return false;
    }

    if (!read(stream.get(), onNextValue, onStopped)) {
        return false;
    }

    return true;
}

bool MicListener::prepare(
        std::shared_ptr<oboe::AudioStream> &stream
) {

    oboe::AudioStreamBuilder streamBuilder;
    streamBuilder.setDirection(oboe::Direction::Input);
    streamBuilder.setPerformanceMode(oboe::PerformanceMode::LowLatency);
    streamBuilder.setFormat(oboe::AudioFormat::I16);
    streamBuilder.setChannelCount(oboe::ChannelCount::Mono);

    auto openingStreamResult = streamBuilder.openStream(stream);
    if (openingStreamResult != oboe::Result::OK) {
        LOGE("Unable to open microphone stream: %s", oboe::convertToText(openingStreamResult));
        return false;
    }

    auto startingStreamResult = stream->start();
    if (startingStreamResult != oboe::Result::OK) {
        LOGE("Unable to start recording: %s", oboe::convertToText(startingStreamResult));
        return false;
    }

    return true;
}

bool MicListener::read(
        oboe::AudioStream *stream,
        const OnNextValueCallback &onNextValue,
        const MicListener::OnFinishedCallback &onStopped
) {

    SamplesPerSecond samplesPerSecond = stream->getSampleRate();
    if (samplesPerSecond <= 0) {
        LOGD("Incorrect samples per second value: %d", samplesPerSecond);
        return false;
    }

    size_t bufferLength = onePortionDurationMs * samplesPerSecond / oboe::kMillisPerSecond;
    Value buffer[bufferLength];

    while (true) {
        std::lock_guard<std::mutex> lock(isActiveSyncMutex_);
        if (!isActive) {
            onStopped();
            bool finishResult = finish(stream);
            return finishResult;
        }
        bool readNextResult = readNext(
                stream, buffer, bufferLength,
                samplesPerSecond, onNextValue
        );
        if (!readNextResult) {
            return false;
        }
    }
}

bool MicListener::readNext(
        oboe::AudioStream *stream,
        Value *buffer,
        size_t bufferSize,
        SamplesPerSecond samplesPerSecond,
        const OnNextValueCallback &onNextValue
) {
    DurationNS timeout = oboe::kNanosPerSecond;
    auto readResult = stream->read(buffer, bufferSize, timeout);
    if (readResult != oboe::Result::OK) {
        LOGE("Unable to read next portion of data: %s", oboe::convertToText(readResult.error()));
        return false;
    }
    auto readFrames = readResult.value();
    for (size_t frame = 0; frame < readFrames; frame++) {
        onNextValue(buffer[frame], samplesPerSecond);
    }
    return true;
}

bool MicListener::finish(
        oboe::AudioStream *stream
) {
    auto stoppingStreamResult = stream->stop();
    if (stoppingStreamResult != oboe::Result::OK) {
        LOGE("Unable to stop recording: %s", oboe::convertToText(stoppingStreamResult));
        return false;
    }
    auto closingStreamResult = stream->close();
    if (closingStreamResult != oboe::Result::OK) {
        LOGE("Unable to close recording stream: %s", oboe::convertToText(closingStreamResult));
        return false;
    }
    return true;
}

bool MicListener::stop() {
    isActiveSyncMutex_.lock();
    isActive = false;
    isActiveSyncMutex_.unlock();
    return true;
}