#ifndef OBOERECORDER_ECHOSOUNDEFFECT_H
#define OBOERECORDER_ECHOSOUNDEFFECT_H

#include <cstdint>
#include <memory>

#include "SoundEffect.h"

namespace audio {

    class EchoSoundEffect : public SoundEffect {

    public:

        EchoSoundEffect(
                DurationMS echoDurationMs, //Paise before echo repeating
                float newValueSignificance = 0.5 //Old and new values mixing factor
        );

        virtual ~EchoSoundEffect() override;

        virtual Value apply(
                Value value,
                SamplesPerSecond samplesPerSecond
        ) override;

    private:

        //Buffer to store previous values
        size_t bufferSize_ = 0;
        Value *buffer_ = nullptr;

        //Buffer uses as circle buffer, so we need position
        size_t positionInBuffer_ = 0;


        DurationMS echoDurationMs_;
        float newValueSignificance_;

        //On the start of effect buffer is empty
        bool isFirstCircle = true;


    };

}


#endif //OBOERECORDER_ECHOSOUNDEFFECT_H
