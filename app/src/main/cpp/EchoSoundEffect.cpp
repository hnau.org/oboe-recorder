#include "EchoSoundEffect.h"

#include <oboe/Oboe.h>

using namespace audio;

EchoSoundEffect::EchoSoundEffect(
        DurationMS echoDurationMs,
        float newValueSignificance
)
        : echoDurationMs_(echoDurationMs),
          newValueSignificance_(newValueSignificance) {
}

EchoSoundEffect::~EchoSoundEffect() {
    delete[] buffer_;
}

Value EchoSoundEffect::apply(
        Value value,
        SamplesPerSecond samplesPerSecond
) {

    if (buffer_ == nullptr) {
        bufferSize_ = echoDurationMs_ * samplesPerSecond / oboe::kMillisPerSecond;
        buffer_ = new Value[bufferSize_];
    }

    if (bufferSize_ <= 0) {
        return value;
    }

    Value result;
    if (isFirstCircle) {
        result = value;
    } else {
        Value oldValue = buffer_[positionInBuffer_];
        result = oldValue + (Value) ((float) (value - oldValue) * newValueSignificance_);
    }
    buffer_[positionInBuffer_] = result;

    positionInBuffer_++;
    if (positionInBuffer_ == bufferSize_) {
        positionInBuffer_ = 0;
        isFirstCircle = false;
    }
    return result;
}
