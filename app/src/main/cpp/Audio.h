#ifndef OBOERECORDER_AUDIO_H
#define OBOERECORDER_AUDIO_H

#include <functional>

namespace audio {

    //Sample value
    typedef int16_t Value;

    typedef int64_t DurationMS;
    typedef int64_t DurationNS;

    typedef int32_t SamplesPerSecond;

}


#endif //OBOERECORDER_AUDIO_H
