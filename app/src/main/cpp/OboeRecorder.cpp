#include "OboeRecorder.h"

#include <fstream>

#include "WavWriter.h"
#include "EchoSoundEffect.h"

using namespace audio;

OboeRecorder::OboeRecorder(
        const char *filePath
) : filePath_(filePath) {

}

bool OboeRecorder::recordSync() {

    auto file = std::make_shared<std::ofstream>();
    file->open(filePath_, std::ios::binary);

    audio::WavWriter wavWriter(file);

    EchoSoundEffect soundEffect(500);

    return micListener.readSync(
            [&soundEffect, &wavWriter](Value nextValue, SamplesPerSecond samplesPerSecond) {
                Value valueWithEffect = soundEffect.apply(nextValue, samplesPerSecond);
                wavWriter.onNextValue(valueWithEffect, samplesPerSecond);
            },
            [&wavWriter]() {
                wavWriter.onNoMoreValues();
            }
    );

}

bool OboeRecorder::stop() {
    return micListener.stop();
}
