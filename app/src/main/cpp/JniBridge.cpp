#include <jni.h>
#include <oboe/Oboe.h>

#include "OboeRecorder.h"
#include "LoggingMacros.h"
#include "WavWriter.h"
#include "Audio.h"

using namespace audio;

extern "C" {

namespace {

    jboolean boolToJBoolean(bool value) {
        return value ? JNI_TRUE : JNI_FALSE;
    }

}

JNIEXPORT jlong JNICALL
Java_org_hnau_oboerecorder_repository_oboe_OboeRecorderNativeLibBridge_prepare(
        JNIEnv *env,
        jobject thiz,
        jstring file_path
) {
    const char *filePath = (*env).GetStringUTFChars(file_path, nullptr);
    return reinterpret_cast<jlong>(new OboeRecorder(filePath));
}

JNIEXPORT jboolean JNICALL
Java_org_hnau_oboerecorder_repository_oboe_OboeRecorderNativeLibBridge_recordSync(
        JNIEnv *env,
        jobject thiz,
        jlong recorder_id
) {
    auto *recorder = reinterpret_cast<OboeRecorder *>(recorder_id);
    return boolToJBoolean(recorder->recordSync());
}

JNIEXPORT jboolean JNICALL
Java_org_hnau_oboerecorder_repository_oboe_OboeRecorderNativeLibBridge_stop(
        JNIEnv *env,
        jobject thiz,
        jlong recorder_id
) {
    auto *recorder = reinterpret_cast<OboeRecorder *>(recorder_id);
    bool result = recorder->stop();
    return boolToJBoolean(result);
}

JNIEXPORT jboolean JNICALL
Java_org_hnau_oboerecorder_repository_oboe_OboeRecorderNativeLibBridge_destroy(
        JNIEnv *env,
        jobject thiz,
        jlong recorder_id
) {
    auto *recorder = reinterpret_cast<OboeRecorder *>(recorder_id);
    try {
        delete recorder;
    } catch (...) {
        return JNI_FALSE;
    }
    return JNI_TRUE;
}

}