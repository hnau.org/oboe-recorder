#include "WavWriter.h"

#include <fstream>

using namespace audio;


namespace {

    constexpr uint8_t valueBytesCount = sizeof(Value);
    constexpr uint8_t channelsCount = 1;
    constexpr uint8_t bitsInByteCount = 8;
    constexpr uint8_t pcmCode = 1;
    constexpr uint8_t pcmSubchunkSize = 16;
    constexpr uint8_t riffStrBytesCount = 4;
    constexpr uint8_t dataStrBytesCount = 4;
    constexpr uint8_t streamLengthBytesCount = 4;

    template<typename T>
    void writeValue(std::ostream &outs, T value, unsigned size) {
        for (; size > 0; size--) {
            outs.put(static_cast <char> (value & 0xFF));
            value = value >> 8;
        }
    }

}

WavWriter::WavWriter(
        std::shared_ptr<std::ofstream> destination
) : destination_(std::move(destination)) {
}

void WavWriter::onNextValue(
        Value value,
        SamplesPerSecond samplesPerSecond
) {
    if (!headerWasWritten_) {
        writeHeader(samplesPerSecond);
        headerWasWritten_ = true;
    }
    writeValue(*destination_, value, valueBytesCount);
}

void WavWriter::onNoMoreValues() {
    fillSizes();
    destination_->flush();
    destination_->close();
}

void WavWriter::fillSizes() {
    size_t totlaLength = destination_->tellp();
    destination_->seekp(dataChunkPos_ + dataStrBytesCount);
    writeValue(
            *destination_,
            totlaLength - dataChunkPos_ + dataStrBytesCount + streamLengthBytesCount,
            streamLengthBytesCount
    );
    destination_->seekp(riffStrBytesCount);
    writeValue(
            *destination_,
            totlaLength - riffStrBytesCount - streamLengthBytesCount,
            streamLengthBytesCount
    );
}

void WavWriter::writeHeader(size_t samplesPerSecond) {
    size_t bytesPerSecond = samplesPerSecond * valueBytesCount * channelsCount;
    destination_->write("RIFF----WAVEfmt ", 16);
    writeValue(*destination_, pcmSubchunkSize, 4);
    writeValue(*destination_, pcmCode, 2);
    writeValue(*destination_, channelsCount, 2);
    writeValue(*destination_, samplesPerSecond, 4);
    writeValue(*destination_, bytesPerSecond, 4);
    writeValue(*destination_, channelsCount * valueBytesCount, 2);
    writeValue(*destination_, valueBytesCount * bitsInByteCount, 2);
    dataChunkPos_ = destination_->tellp();
    destination_->write("data----", 8);
}
