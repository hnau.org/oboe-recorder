package org.hnau.oboerecorder.di

import android.content.Context
import dagger.Component
import org.hnau.oboerecorder.App
import org.hnau.oboerecorder.ui.di.UIScopeComponent
import org.hnau.oboerecorder.ui.mainactivity.MainActivityModule
import javax.inject.Singleton


/**
 * Components, which provides app scope dependencies
 */
@Singleton
@Component(
    modules = [
        AppModule::class,
        AppRepositoriesModule::class
    ]
)
abstract class AppComponent {

    companion object {

        fun create(
            app: App
        ): AppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(app))
            .build()

    }

    abstract val context: Context

    abstract fun createUIScopeComponent(
        mainActivityModule: MainActivityModule
    ): UIScopeComponent

}