package org.hnau.oboerecorder.di

import android.content.Context
import dagger.Module
import dagger.Provides


/**
 * Module, which provides base app dependencies
 */
@Module
class AppModule(
    @get:Provides
    val context: Context
)