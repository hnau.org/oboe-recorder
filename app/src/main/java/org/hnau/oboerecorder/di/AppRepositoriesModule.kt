package org.hnau.oboerecorder.di

import dagger.Module
import dagger.Provides
import org.hnau.oboerecorder.domain.repository.RecordRepository
import org.hnau.oboerecorder.repository.oboe.OboeRecordRepository
import org.hnau.oboerecorder.repository.recordfile.RecordFileBaseInfo
import org.hnau.oboerecorder.repository.recordfile.RecordFileBaseInfoImpl
import javax.inject.Singleton


/**
 * Module, which provides app scope repositories
 */
@Module
class AppRepositoriesModule {

    @Singleton
    @Provides
    fun getRecordFileBaseInfo(
        recordFileBaseInfoImpl: RecordFileBaseInfoImpl
    ): RecordFileBaseInfo = recordFileBaseInfoImpl

    @Singleton
    @Provides
    fun getRecordRepository(
        oboeRecordRepository: OboeRecordRepository
    ): RecordRepository = oboeRecordRepository

}