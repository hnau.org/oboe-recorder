package org.hnau.oboerecorder.repository.recordfile

import android.content.Context
import java.io.File
import javax.inject.Inject


/**
 * Local app files implementation of [RecordFileBaseInfo]
 */
class RecordFileBaseInfoImpl @Inject constructor(
    private val context: Context
) : RecordFileBaseInfo {

    companion object {

        private const val localPath = "records"
        private const val fileName = "record.wav"

    }

    private val dir = File(context.filesDir, localPath)

    override val file = File(dir, fileName)

    init {
        dir.mkdirs()
    }

}