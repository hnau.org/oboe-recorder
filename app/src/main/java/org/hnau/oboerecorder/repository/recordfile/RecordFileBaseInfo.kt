package org.hnau.oboerecorder.repository.recordfile

import java.io.File


/**
 * Provides file to record in
 */
interface RecordFileBaseInfo {

    /**
     * File to record in
     */
    val file: File

}