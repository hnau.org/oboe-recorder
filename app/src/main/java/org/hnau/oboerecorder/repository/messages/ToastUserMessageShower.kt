package org.hnau.oboerecorder.repository.messages

import android.content.Context
import android.widget.Toast
import javax.inject.Inject

/**
 * User message [Toast] shower
 */
class ToastUserMessageShower @Inject constructor(
    private val context: Context
) : UserMessageShower {

    override fun showMessageToUser(message: String) =
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

}