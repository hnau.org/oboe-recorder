package org.hnau.oboerecorder.repository.messages

/**
 * Message to user shower
 */
interface UserMessageShower {

    fun showMessageToUser(
        message: String
    )

}