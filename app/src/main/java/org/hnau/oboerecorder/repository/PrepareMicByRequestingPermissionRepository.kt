package org.hnau.oboerecorder.repository

import org.hnau.oboerecorder.domain.repository.PrepareMicRepository
import org.hnau.oboerecorder.utils.permissions.PermissionsRequester
import javax.inject.Inject


/**
 * Implementation of [PrepareMicRepository]
 * based on requesting [android.Manifest.permission.RECORD_AUDIO] permission
 */
class PrepareMicByRequestingPermissionRepository @Inject constructor(
    private val permissionsRequester: PermissionsRequester
) : PrepareMicRepository {

    override fun prepareMic() =
        permissionsRequester.requestPermission(android.Manifest.permission.RECORD_AUDIO)

}