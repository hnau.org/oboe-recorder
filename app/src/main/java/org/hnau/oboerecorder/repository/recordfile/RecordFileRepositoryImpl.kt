package org.hnau.oboerecorder.repository.recordfile

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.core.content.FileProvider
import org.hnau.oboerecorder.R
import org.hnau.oboerecorder.domain.entity.listen.RecordedFileStatus
import org.hnau.oboerecorder.domain.repository.RecordFileRepository
import org.hnau.oboerecorder.repository.messages.UserMessageShower
import org.hnau.oboerecorder.ui.mainactivity.ActivityContextQualifier
import java.lang.Exception
import javax.inject.Inject


/**
 * Implementation of [RecordFileRepository] based on [RecordFileBaseInfo]
 */
class RecordFileRepositoryImpl @Inject constructor(
    private val recordFileBaseInfo: RecordFileBaseInfo,
    @ActivityContextQualifier private val context: Context,
    private val messageShower: UserMessageShower
) : RecordFileRepository {

    companion object {

        private const val authority = "org.hnau.oboerecorder"

    }

    private val existsStatus by lazy {
        RecordedFileStatus.Exists {

            val contentProviderUri: Uri = FileProvider.getUriForFile(
                context, authority, recordFileBaseInfo.file
            )

            val intent = Intent().apply {
                action = Intent.ACTION_VIEW
                data = contentProviderUri
                flags = Intent.FLAG_ACTIVITY_NO_HISTORY or
                        Intent.FLAG_GRANT_READ_URI_PERMISSION
            }

            try {
                context.startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                messageShower.showMessageToUser(
                    context.getString(R.string.user_error_message_no_music_apps)
                )
            }
        }

    }

    override val status: RecordedFileStatus
        get() = if (recordFileBaseInfo.file.exists()) {
            existsStatus
        } else {
            RecordedFileStatus.NotExists
        }

}