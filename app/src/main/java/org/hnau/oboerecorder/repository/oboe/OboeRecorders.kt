package org.hnau.oboerecorder.repository.oboe

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import org.hnau.oboerecorder.utils.Box
import java.util.concurrent.Executors


/**
 * From microphone to file many times recorder
 * @param filePath path of the file to record
 */
class OboeRecorders(
    private val filePath: String
) {


    sealed class Status {

        object Preparing : Status()

        data class Recording(
            val stop: () -> Unit
        ) : Status()

        data class Ready(
            val previousRecordingIsSuccess: Boolean,
            val start: () -> Unit
        ) : Status()

        companion object {

            fun fromRecorderStatus(
                recorderStatus: OboeRecorder.Status,
                startIfFinished: () -> Unit
            ) = when (recorderStatus) {
                is OboeRecorder.Status.Finished -> Ready(recorderStatus.success, startIfFinished)
                OboeRecorder.Status.Initializing -> Preparing
                is OboeRecorder.Status.Recording -> Recording(recorderStatus.stop)
            }

        }

    }

    private val currentRecorder: BehaviorSubject<Box<OboeRecorder?>> =
        BehaviorSubject.createDefault(Box(null))

    private val noRecorderStatus: BehaviorSubject<Status> =
        BehaviorSubject.createDefault(
            Status.Ready(true, ::swhichToNextRecorder)
        )

    /**
     * Current recording status
     */
    val status: Observable<Status> =
        currentRecorder.switchMap { recorder ->
            recorder
                ?.value
                ?.status
                ?.map { recorderStatus ->
                    Status.fromRecorderStatus(recorderStatus, ::swhichToNextRecorder)
                }
                ?: noRecorderStatus
        }

    private val executor = Executors.newCachedThreadPool()

    private fun swhichToNextRecorder() =
        currentRecorder.onNext(Box(OboeRecorder(filePath) { executor.submit(it) }))

}