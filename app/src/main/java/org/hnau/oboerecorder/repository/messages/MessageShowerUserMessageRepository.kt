package org.hnau.oboerecorder.repository.messages

import android.content.Context
import org.hnau.oboerecorder.R
import org.hnau.oboerecorder.domain.repository.UserMessagesRepository
import org.hnau.oboerecorder.utils.permissions.PermissionDeniedException
import javax.inject.Inject


/**
 * Message to user through [UserMessageShower] shower
 */
class MessageShowerUserMessageRepository @Inject constructor(
    private val context: Context,
    private val userMessageShower: UserMessageShower
) : UserMessagesRepository {

    private val defaultMessage
            by lazy { context.getString(R.string.user_error_message_default) }

    override fun showError(error: Throwable) {
        val message = getMessage(error)
        userMessageShower.showMessageToUser(message)
    }

    private fun getMessage(
        error: Throwable
    ) = when (error) {
        is PermissionDeniedException -> {
            if (error.permission == android.Manifest.permission.RECORD_AUDIO) {
                context.getString(R.string.user_error_message_record_audio_permission_denied)
            } else {
                defaultMessage
            }
        }
        else -> defaultMessage
    }

}