package org.hnau.oboerecorder.repository.oboe


/**
 * JNI adapter for native recording functions
 */
object OboeRecorderNativeLibBridge {

    init {
        System.loadLibrary("oboerecorder")
    }

    external fun prepare(
        filePath: String
    ): Long

    external fun recordSync(
        recorderId: Long
    ): Boolean

    external fun stop(
        recorderId: Long
    ): Boolean

    external fun destroy(
        recorderId: Long
    ): Boolean

}