package org.hnau.oboerecorder.repository.oboe

import io.reactivex.rxjava3.core.Observable
import org.hnau.oboerecorder.domain.entity.record.RecordStatus
import org.hnau.oboerecorder.domain.repository.RecordRepository
import org.hnau.oboerecorder.repository.recordfile.RecordFileBaseInfo
import javax.inject.Inject


/**
 * Oboe implementation of [RecordRepository]
 */
class OboeRecordRepository @Inject constructor(
    private val recordFileBaseInfo: RecordFileBaseInfo
) : RecordRepository {

    private val recorders = OboeRecorders(recordFileBaseInfo.file.path)

    override val status: Observable<RecordStatus> =
        recorders.status.map { recordersStatus ->
            when (recordersStatus) {
                OboeRecorders.Status.Preparing -> RecordStatus.Preparing
                is OboeRecorders.Status.Ready -> RecordStatus.Ready(
                    previousRecordingIsSuccess = recordersStatus.previousRecordingIsSuccess,
                    start = recordersStatus.start
                )
                is OboeRecorders.Status.Recording -> RecordStatus.Recording(
                    stop = recordersStatus.stop
                )
            }
        }

}