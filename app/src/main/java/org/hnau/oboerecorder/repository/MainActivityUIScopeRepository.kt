package org.hnau.oboerecorder.repository

import io.reactivex.rxjava3.disposables.DisposableContainer
import org.hnau.oboerecorder.domain.repository.UIScopeRepository
import javax.inject.Inject


/**
 * Implementation of [UIScopeRepository] based on [MainActivity] scope
 */
class MainActivityUIScopeRepository @Inject constructor(
    override val uiScopeDisposable: DisposableContainer
): UIScopeRepository