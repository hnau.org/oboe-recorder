package org.hnau.oboerecorder.repository.oboe

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject


/**
 * From microphone to file recorder
 * @param filePath path of the file to record
 * @param asyncExecutor lambda, which execute action in other [Thread]
 */
class OboeRecorder(
    filePath: String,
    private val asyncExecutor: (() -> Unit) -> Unit
) {

    sealed class Status {

        object Initializing : Status()

        data class Recording(
            val stop: () -> Unit
        ) : Status()

        data class Finished(
            val success: Boolean
        ) : Status() {

            companion object {

                val success = Finished(true)
                val error = Finished(false)

            }

        }

    }

    companion object {

        private const val errorRecorderId = 0L

    }

    private val statusInner: BehaviorSubject<Status> =
        BehaviorSubject.createDefault(Status.Initializing)

    /**
     * Recording status
     */
    val status: Observable<Status>
        get() = statusInner

    init {
        asyncExecutor {
            val recorderId = OboeRecorderNativeLibBridge.prepare(filePath)
            if (recorderId == errorRecorderId) {
                updateStatus(Status.Finished.error)
                return@asyncExecutor
            }

            val isFinishedLock = Any()
            var isFinished = false
            var stopWasCalled = false;
            updateStatus(
                Status.Recording {
                    synchronized(isFinishedLock) {
                        if (!isFinished && !stopWasCalled) {
                            stopWasCalled = true;
                            asyncExecutor {
                                    OboeRecorderNativeLibBridge.stop(recorderId)
                            }
                        }
                    }
                }
            )

            val recordingIsSuccess = OboeRecorderNativeLibBridge.recordSync(recorderId)
            synchronized(isFinishedLock) {
                isFinished = true
            }
            val finishedIsSuccess = OboeRecorderNativeLibBridge.destroy(recorderId)
            updateStatus(Status.Finished(recordingIsSuccess && finishedIsSuccess))
        }
    }

    private fun updateStatus(newStatus: Status) =
        statusInner.onNext(newStatus)


}