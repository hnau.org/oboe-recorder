package org.hnau.oboerecorder.domain.interactors.listen

import io.reactivex.rxjava3.core.Observable
import org.hnau.oboerecorder.domain.entity.listen.AvailableListenAction


/**
 * Interactor, which provide access to listening
 */
interface ListenInteractor {

    /**
     * Available action with listening part of the app
     */
    val availableAction: Observable<AvailableListenAction>

}