package org.hnau.oboerecorder.domain.repository


/**
 * Message to user shower
 */
interface UserMessagesRepository {

    /**
     * Showe error message to user
     */
    fun showError(error: Throwable)

}