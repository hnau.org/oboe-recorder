package org.hnau.oboerecorder.domain.interactors

import org.hnau.oboerecorder.domain.entity.record.RecordStatus
import org.hnau.oboerecorder.domain.repository.PrepareMicRepository
import org.hnau.oboerecorder.domain.repository.RecordRepository
import org.hnau.oboerecorder.domain.repository.UIScopeRepository
import org.hnau.oboerecorder.domain.repository.UserMessagesRepository
import javax.inject.Inject


/**
 * Provides current recording status
 */
class RecordingStatusProvider @Inject constructor(
    private val prepareMicRepository: PrepareMicRepository,
    private val recordRepository: RecordRepository,
    private val userMessagesRepository: UserMessagesRepository,
    private val uiScopeRepository: UIScopeRepository
) {

    /**
     * Current recording status
     */
    val status = recordRepository.status.map { status ->
        status.replaceStartAction { defaultStartAction ->
            uiScopeRepository
                .uiScopeDisposable
                .add(
                    prepareMicRepository
                        .prepareMic()
                        .subscribe(
                            { defaultStartAction() },
                            { userMessagesRepository.showError(it) }
                        )
                )
        }
    }

    private inline fun RecordStatus.replaceStartAction(
        crossinline newStartAction: (oldStartAction: () -> Unit) -> Unit
    ) = when (this) {
        is RecordStatus.Ready -> copy {
            newStartAction(start)
        }
        else -> this
    }

}