package org.hnau.oboerecorder.domain.repository

import io.reactivex.rxjava3.disposables.DisposableContainer


/**
 * Current UI scope disposable provider
 */
interface UIScopeRepository {
    /**
     * Current UI scope disposable
     */
    val uiScopeDisposable: DisposableContainer

}