package org.hnau.oboerecorder.domain.entity.record

/**
 * Current recording status
 */
sealed class RecordStatus {

    object Preparing : RecordStatus()

    data class Recording(
        val stop: () -> Unit
    ) : RecordStatus()

    data class Ready(
        val previousRecordingIsSuccess: Boolean,
        val start: () -> Unit
    ) : RecordStatus()

}