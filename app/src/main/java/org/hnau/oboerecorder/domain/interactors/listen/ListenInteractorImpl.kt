package org.hnau.oboerecorder.domain.interactors.listen

import org.hnau.oboerecorder.domain.entity.listen.AvailableListenAction
import org.hnau.oboerecorder.domain.entity.listen.RecordedFileStatus
import org.hnau.oboerecorder.domain.entity.record.RecordStatus
import org.hnau.oboerecorder.domain.interactors.RecordingStatusProvider
import org.hnau.oboerecorder.domain.repository.RecordFileRepository
import javax.inject.Inject


class ListenInteractorImpl @Inject constructor(
    private val recordingStatusProvider: RecordingStatusProvider,
    private val recordFileRepository: RecordFileRepository
) : ListenInteractor {

    companion object {

        private val noAction by lazy {
            AvailableListenAction(
                type = AvailableListenAction.Type.none,
                action = null
            )
        }

    }

    override val availableAction = recordingStatusProvider
        .status
        .map { recordStatus ->
            if (recordStatus !is RecordStatus.Ready) {
                return@map noAction
            }
            when (val status = recordFileRepository.status) {
                is RecordedFileStatus.Exists -> AvailableListenAction(
                    type = AvailableListenAction.Type.listen,
                    action = status.listen
                )
                RecordedFileStatus.NotExists -> noAction
            }
        }
        .distinctUntilChanged()

}