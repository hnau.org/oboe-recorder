package org.hnau.oboerecorder.domain.entity.listen


/**
 * Provides status of recorded file
 */
sealed class RecordedFileStatus {

    object NotExists : RecordedFileStatus()

    data class Exists(
        val listen: () -> Unit
    ) : RecordedFileStatus()

}