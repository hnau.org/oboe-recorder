package org.hnau.oboerecorder.domain.repository

import io.reactivex.rxjava3.core.Observable
import org.hnau.oboerecorder.domain.entity.record.RecordStatus


/**
 * Recording status provider
 */
interface RecordRepository {

    /**
     * Current recording status
     */
    val status: Observable<RecordStatus>

}