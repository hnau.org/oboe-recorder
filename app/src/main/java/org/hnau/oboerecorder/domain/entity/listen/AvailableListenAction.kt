package org.hnau.oboerecorder.domain.entity.listen


/**
 * Action, which can be executed with recorded file
 */
data class AvailableListenAction(
    val type: Type,
    val action: (() -> Unit)? = null
) {

    enum class Type {
        listen,
        none
    }

}