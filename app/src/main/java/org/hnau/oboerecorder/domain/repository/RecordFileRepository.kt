package org.hnau.oboerecorder.domain.repository

import org.hnau.oboerecorder.domain.entity.listen.RecordedFileStatus


/**
 * Recorded file status provider
 */
interface RecordFileRepository {

    /**
     * Status of recorded file
     */
    val status: RecordedFileStatus

}