package org.hnau.oboerecorder.domain.interactors.record

import org.hnau.oboerecorder.domain.entity.record.AvailableRecordAction
import org.hnau.oboerecorder.domain.entity.record.RecordStatus
import org.hnau.oboerecorder.domain.entity.record.RecordingStatusView
import org.hnau.oboerecorder.domain.interactors.RecordingStatusProvider
import javax.inject.Inject


class RecordInteractorImpl @Inject constructor(
    private val recordingStatusProvider: RecordingStatusProvider
) : RecordInteractor {

    override val isRecording = recordingStatusProvider
        .status
        .map { status ->
            status !is RecordStatus.Ready
        }
        .distinctUntilChanged()

    override val statusView = recordingStatusProvider
        .status
        .map { status ->
            when (status) {
                RecordStatus.Preparing -> RecordingStatusView.preparing
                is RecordStatus.Ready -> {
                    if (status.previousRecordingIsSuccess) {
                        RecordingStatusView.ready
                    } else {
                        RecordingStatusView.error
                    }
                }
                is RecordStatus.Recording -> RecordingStatusView.recording
            }
        }

    override val availableAction = recordingStatusProvider
        .status
        .map { status ->
            when (status) {
                RecordStatus.Preparing -> AvailableRecordAction(
                    AvailableRecordAction.Type.none,
                    {}
                )
                is RecordStatus.Ready -> AvailableRecordAction(
                    AvailableRecordAction.Type.start,
                    { status.start() }
                )
                is RecordStatus.Recording -> AvailableRecordAction(
                    AvailableRecordAction.Type.stop,
                    { status.stop() }
                )
            }
        }

}