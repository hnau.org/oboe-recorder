package org.hnau.oboerecorder.domain.entity.record


/**
 * Current recording view status
 */
enum class RecordingStatusView {
    preparing,
    recording,
    ready,
    error
}
