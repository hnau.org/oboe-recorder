package org.hnau.oboerecorder.domain.interactors.record

import io.reactivex.rxjava3.core.Observable
import org.hnau.oboerecorder.domain.entity.record.AvailableRecordAction
import org.hnau.oboerecorder.domain.entity.record.RecordingStatusView

/**
 * Interactor, which provide access to recording
 */
interface RecordInteractor {

    /**
     * Is recording now
     */
    val isRecording: Observable<Boolean>

    /**
     * Current recording view status
     */
    val statusView: Observable<RecordingStatusView>

    /**
     * Available action with recording part of the app
     */
    val availableAction: Observable<AvailableRecordAction>

}