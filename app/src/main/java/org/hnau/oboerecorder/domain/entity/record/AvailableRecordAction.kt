package org.hnau.oboerecorder.domain.entity.record


/**
 * Action, which can be executed with recording part of app
 */
data class AvailableRecordAction(
    val type: Type,
    val action: (() -> Unit)? = null
) {

    enum class Type {
        start,
        stop,
        none
    }

}