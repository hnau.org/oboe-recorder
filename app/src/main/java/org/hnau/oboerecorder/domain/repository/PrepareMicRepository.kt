package org.hnau.oboerecorder.domain.repository

import io.reactivex.rxjava3.core.Completable


/**
 * Before recording microphone preparer
 */
interface PrepareMicRepository {

    /**
     * Prepare microphone before recording
     */
    fun prepareMic(): Completable

}