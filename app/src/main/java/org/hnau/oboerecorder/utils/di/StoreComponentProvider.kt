package org.hnau.oboerecorder.utils.di


/**
 * Implementation of [ComponentProvider], which stores component
 */
abstract class StoreComponentProvider<C : Any> : ComponentProvider<C> {

    override final var component: C? = null
        private set

    protected fun fill(
        component: C
    ) {
        this.component = component
    }

    fun clear() {
        this.component = null
    }

}