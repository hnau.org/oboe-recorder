package org.hnau.oboerecorder.utils


/**
 * Simple value container. Used as wrapper for nullable value in BehaviorSubject
 */
data class Box<out T>(
    val value: T
)