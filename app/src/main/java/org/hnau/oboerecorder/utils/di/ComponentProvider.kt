package org.hnau.oboerecorder.utils.di


/**
 * Provides some Dagger Component
 */
interface ComponentProvider<C : Any> {

    val component: C?

}

val <C : Any> ComponentProvider<C>.componentOrThrow
    get() = component!!