package org.hnau.oboerecorder.utils.permissions

import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.subjects.CompletableSubject


class PermissionsRequester(
    private val activity: Activity
) {

    private val results = HashMap<Int, CompletableSubject>()

    fun requestPermission(
        permission: String
    ): Completable {

        val checkResult = ContextCompat.checkSelfPermission(activity, permission)
        if (checkResult == PackageManager.PERMISSION_GRANTED) {
            return Completable.complete()
        }

        val result = CompletableSubject.create()
        val requestCode = placeCompletable(result)
        ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)

        return result
    }

    @Suppress("UNUSED_PARAMETER")
    fun onPermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        val completable = results[requestCode] ?: return
        val result = grantResults
            .firstOrNull()
            ?.let { it == PackageManager.PERMISSION_GRANTED }
            ?: error("Expected at least one result")
        if (result) {
            completable.onComplete()
        } else {
            val permission = permissions.firstOrNull() ?: "Unknown"
            val exception = PermissionDeniedException(permission)
            completable.onError(exception)
        }
    }

    private fun placeCompletable(
        completable: CompletableSubject
    ) = synchronized(results) {
        val existingCodes = results.keys
        var requestCode = 1;
        while (requestCode in existingCodes) {
            requestCode++
        }
        results[requestCode] = completable
        return@synchronized requestCode
    }

}