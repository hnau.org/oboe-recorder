package org.hnau.oboerecorder.utils.permissions


class PermissionDeniedException(
    val permission: String
) : RuntimeException(
    "Permission $permission denied"
)