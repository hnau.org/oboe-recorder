package org.hnau.oboerecorder

import android.app.Application
import org.hnau.oboerecorder.di.AppComponent


class App : Application() {

    companion object {

        lateinit var component: AppComponent
            private set

    }

    init {
        component = AppComponent.create(this)
    }

}