package org.hnau.oboerecorder.ui.mainactivity

import android.content.Context
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.disposables.DisposableContainer
import org.hnau.oboerecorder.ui.di.UIScope
import org.hnau.oboerecorder.utils.permissions.PermissionsRequester

/**
 * Module, which provides dependencies from MainActivity
 */
@Module
class MainActivityModule(
    private val mainActivity: MainActivity
) {

    @get:[Provides ActivityContextQualifier]
    val context: Context
        get() = mainActivity

    @get:Provides
    val permissionsRequester: PermissionsRequester
        get() = mainActivity.permissionsRequester

    @get:Provides
    val uiScopeDisposable: DisposableContainer
        get() = mainActivity.uiScopeDisposable

}