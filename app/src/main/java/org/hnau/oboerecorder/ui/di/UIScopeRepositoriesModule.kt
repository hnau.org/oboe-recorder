package org.hnau.oboerecorder.ui.di

import dagger.Module
import dagger.Provides
import org.hnau.oboerecorder.domain.repository.PrepareMicRepository
import org.hnau.oboerecorder.domain.repository.RecordFileRepository
import org.hnau.oboerecorder.domain.repository.UIScopeRepository
import org.hnau.oboerecorder.domain.repository.UserMessagesRepository
import org.hnau.oboerecorder.repository.MainActivityUIScopeRepository
import org.hnau.oboerecorder.repository.PrepareMicByRequestingPermissionRepository
import org.hnau.oboerecorder.repository.messages.MessageShowerUserMessageRepository
import org.hnau.oboerecorder.repository.messages.ToastUserMessageShower
import org.hnau.oboerecorder.repository.messages.UserMessageShower
import org.hnau.oboerecorder.repository.recordfile.RecordFileRepositoryImpl

/**
 * Module, which provides [UIScope] repositories
 */
@Module
class UIScopeRepositoriesModule {

    @Provides
    @UIScope
    fun getPrepareMicRepository(
        prepareMicByRequestingPermissionRepository: PrepareMicByRequestingPermissionRepository
    ): PrepareMicRepository = prepareMicByRequestingPermissionRepository

    @Provides
    @UIScope
    fun getUIScopeRepository(
        mainActivityUIScopeRepository: MainActivityUIScopeRepository
    ): UIScopeRepository = mainActivityUIScopeRepository

    @Provides
    @UIScope
    fun getUserMessagesRepository(
        messageShowerUserMessageRepository: MessageShowerUserMessageRepository
    ): UserMessagesRepository = messageShowerUserMessageRepository

    @Provides
    @UIScope
    fun getUserMessageShower(
        toastUserMessageShower: ToastUserMessageShower
    ): UserMessageShower = toastUserMessageShower

    @Provides
    @UIScope
    fun getRecordFileRepository(
        recordFileRepositoryImpl: RecordFileRepositoryImpl
    ): RecordFileRepository = recordFileRepositoryImpl

}