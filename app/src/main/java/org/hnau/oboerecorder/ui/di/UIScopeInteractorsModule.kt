package org.hnau.oboerecorder.ui.di

import dagger.Module
import dagger.Provides
import org.hnau.oboerecorder.domain.interactors.listen.ListenInteractor
import org.hnau.oboerecorder.domain.interactors.listen.ListenInteractorImpl
import org.hnau.oboerecorder.domain.interactors.record.RecordInteractor
import org.hnau.oboerecorder.domain.interactors.record.RecordInteractorImpl


/**
 * Module, which provides [UIScope] interactors
 */
@Module
class UIScopeInteractorsModule {

    @UIScope
    @Provides
    fun getRecordInteractor(
        recordInteractorImpl: RecordInteractorImpl
    ): RecordInteractor = recordInteractorImpl

    @UIScope
    @Provides
    fun getListenInteractor(
        listenInteractorImpl: ListenInteractorImpl
    ): ListenInteractor = listenInteractorImpl

}