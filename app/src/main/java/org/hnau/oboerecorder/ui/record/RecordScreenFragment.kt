package org.hnau.oboerecorder.ui.record

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.view.isInvisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import io.reactivex.rxjava3.disposables.CompositeDisposable
import org.hnau.oboerecorder.R


class RecordScreenFragment : Fragment(R.layout.record_screen_fragment) {

    private val viewModel: RecordScreenViewModel by viewModels()

    private var viewScopeDisposable = CompositeDisposable()
        set(value) {
            field.clear()
            field = value
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewScopeDisposable = CompositeDisposable()

        view.findViewById<View>(R.id.progress_is_active).apply {
            viewScopeDisposable.add(
                viewModel.isRecording.subscribe { isRecording ->
                    isInvisible = !isRecording
                }
            )
        }

        view.findViewById<TextView>(R.id.text_status).apply {
            viewScopeDisposable.add(
                viewModel.statusMessage.subscribe { statusMessage ->
                    setText(statusMessage)
                }
            )
        }

        view.findViewById<TextView>(R.id.button_record).apply {
            viewScopeDisposable.add(
                viewModel.recordButtonInfo.subscribe { (title, onClickListener, isEnabled) ->
                    setText(title)
                    setOnClickListener { onClickListener() }
                    setEnabled(isEnabled)
                }
            )
        }

        view.findViewById<View>(R.id.button_listen).apply {
            viewScopeDisposable.add(
                viewModel.listendButtonInfo.subscribe { (onClickListener, isEnabled) ->
                    setOnClickListener { onClickListener() }
                    setEnabled(isEnabled)
                }
            )
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewScopeDisposable.clear()
    }

}