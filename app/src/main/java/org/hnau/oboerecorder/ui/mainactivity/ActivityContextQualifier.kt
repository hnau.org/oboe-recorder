package org.hnau.oboerecorder.ui.mainactivity

import javax.inject.Qualifier


/**
 * [Qualifier], which describes Activity (no Application) Context
 */
@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class ActivityContextQualifier()
