package org.hnau.oboerecorder.ui.di

import dagger.Subcomponent
import org.hnau.oboerecorder.App
import org.hnau.oboerecorder.ui.mainactivity.MainActivity
import org.hnau.oboerecorder.ui.mainactivity.MainActivityModule
import org.hnau.oboerecorder.ui.record.RecordScreenViewModel
import org.hnau.oboerecorder.utils.di.StoreComponentProvider


/**
 * Component, which provides UIScope dependencies
 */
@UIScope
@Subcomponent(
    modules = [
        UIScopeInteractorsModule::class,
        UIScopeRepositoriesModule::class,
        MainActivityModule::class
    ]
)
interface UIScopeComponent {

    companion object : StoreComponentProvider<UIScopeComponent>() {

        fun fill(
            mainActivity: MainActivity
        ) = fill(
            component = App.component.createUIScopeComponent(
                mainActivityModule = MainActivityModule(
                    mainActivity
                )
            )
        )

    }

    fun inject(dependencies: RecordScreenViewModel.Dependencies)

}