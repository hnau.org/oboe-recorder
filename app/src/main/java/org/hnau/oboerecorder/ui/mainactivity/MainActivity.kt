package org.hnau.oboerecorder.ui.mainactivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.rxjava3.disposables.CompositeDisposable
import org.hnau.oboerecorder.R
import org.hnau.oboerecorder.ui.di.UIScopeComponent
import org.hnau.oboerecorder.utils.permissions.PermissionsRequester


class MainActivity : AppCompatActivity() {

    /**
     * [PermissionsRequester] associated with this activity
     */
    val permissionsRequester = PermissionsRequester(this)

    /**
     * This activity lifecycle disposable
     */
    val uiScopeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        UIScopeComponent.fill(this)
        setContentView(R.layout.main_activity)
    }

    override fun onDestroy() {
        super.onDestroy()
        uiScopeDisposable.clear()
        UIScopeComponent.clear()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionsRequester.onPermissionResult(requestCode, permissions, grantResults)
    }

}