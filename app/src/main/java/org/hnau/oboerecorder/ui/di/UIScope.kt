package org.hnau.oboerecorder.ui.di

import javax.inject.Scope


/**
 * Interface is active scope of application
 */
@Scope
annotation class UIScope()
