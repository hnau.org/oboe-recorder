package org.hnau.oboerecorder.ui.record

import android.content.Context
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.core.Observable
import org.hnau.oboerecorder.R
import org.hnau.oboerecorder.domain.entity.listen.AvailableListenAction
import org.hnau.oboerecorder.domain.entity.record.AvailableRecordAction
import org.hnau.oboerecorder.domain.entity.record.RecordingStatusView
import org.hnau.oboerecorder.domain.interactors.listen.ListenInteractor
import org.hnau.oboerecorder.domain.interactors.record.RecordInteractor
import org.hnau.oboerecorder.ui.di.UIScopeComponent
import org.hnau.oboerecorder.utils.di.componentOrThrow
import org.hnau.oboerecorder.utils.observeOnMainThread
import javax.inject.Inject


class RecordScreenViewModel : ViewModel() {

    class Dependencies {

        @Inject
        lateinit var context: Context

        @Inject
        lateinit var recordInteractor: RecordInteractor

        @Inject
        lateinit var listenInteractor: ListenInteractor

        init {
            UIScopeComponent.componentOrThrow.inject(this)
        }

    }

    private val dependencies = Dependencies()

    /**
     * Is recording now status
     */
    val isRecording =
        dependencies.recordInteractor.isRecording.observeOnMainThread()


    inner class RecordButtonInfo(
        val title: String,
        val onClickListener: () -> Unit,
        val isEnabled: Boolean
    ) {

        constructor(
            titleResId: Int,
            onClickListener: (() -> Unit)? = null
        ) : this(
            title = dependencies.context.getString(titleResId),
            onClickListener = onClickListener ?: {},
            isEnabled = onClickListener != null
        )

        operator fun component1() = title
        operator fun component2() = onClickListener
        operator fun component3() = isEnabled

    }

    /**
     * Record button view info
     */
    val recordButtonInfo =
        dependencies.recordInteractor.availableAction
            .map { (type, action) ->
                RecordButtonInfo(
                    titleResId = when (type) {
                        AvailableRecordAction.Type.start -> R.string.record_button_title_start
                        AvailableRecordAction.Type.stop -> R.string.record_button_title_stop
                        AvailableRecordAction.Type.none -> R.string.record_button_title_preparing
                    },
                    onClickListener = action
                )
            }
            .observeOnMainThread()

    /**
     * Current recording view status
     */
    val statusMessage: Observable<String> =
        dependencies.recordInteractor.statusView
            .map { recordStatusView: RecordingStatusView ->
                val statusText = dependencies.context.getString(
                    when (recordStatusView) {
                        RecordingStatusView.preparing -> R.string.recording_status_title_preparing
                        RecordingStatusView.recording -> R.string.recording_status_title_recording
                        RecordingStatusView.ready -> R.string.recording_status_title_ready
                        RecordingStatusView.error -> R.string.recording_status_title_error
                    }
                )
                dependencies.context.getString(
                    R.string.recording_status_title,
                    statusText
                )
            }
            .observeOnMainThread()

    /**
     * Listen button view info
     */
    data class ListenButtonInfo(
        val onClickListener: () -> Unit,
        val isEnabled: Boolean
    ) {

        companion object {

            val disabled = ListenButtonInfo(null)

        }

        constructor(
            onClickListener: (() -> Unit)?
        ) : this(
            onClickListener = onClickListener ?: {},
            isEnabled = onClickListener != null
        )

    }

    val listendButtonInfo: Observable<ListenButtonInfo> =
        dependencies.listenInteractor.availableAction
            .map { (type, action) ->
                when (type) {
                    AvailableListenAction.Type.listen -> ListenButtonInfo(action)
                    AvailableListenAction.Type.none -> ListenButtonInfo.disabled
                }
            }
            .observeOnMainThread()

}